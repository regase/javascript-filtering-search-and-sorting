(function(){
	"use strict";

	function App() {
		this.concerts = [];
		this.currentConcerts = [];
		var api = new API();
		// Load icelandic language for moment
		moment.locale('is');
		
		// get the concerts and then on success call the updateConcertsTable function
		api.getConcerts($.proxy(this.setupConcerts, this));
		$(".datepicker").datepicker({
			dateFormat: 'dd-mm-yy',
			onSelect: $.proxy(this.applyFilters, this)
  		});
  		$(".datepicker").on('input', $.proxy(this.applyFilters, this));
  		$("#search").on('input', $.proxy(this.applyFilters, this));
  		$(".sort").click($.proxy(this.applyFilters, this));
	}

	App.prototype = {
		constructor: App,
		/* Use this to setup when the data has been received */
		setupConcerts: function (concerts) {
			this.concerts = concerts["results"];
			this.currentConcerts = this.concerts;
			this.updateConcertsTable(this.concerts);
		},
		/* Update the concerts table with the currentConcert */
		updateConcertsTable: function () {
			$(".concerts").empty();
			this.currentConcerts.forEach(function(concert) {
    			$(".concerts").append(
    				'<tr>' +
    					'<td><img class="evimg" src="' + concert["imageSource"] + '"></td>' +
    					'<td>' + concert["eventDateName"] + '</td>' +
    					'<td>' + concert["eventHallName"] + '</td>' +
    					'<td>' + moment(concert["dateOfShow"]).format('LLLL') + '</td>' +
    					'<td>' + concert["userGroupName"] + '</td>' +
    				'</tr>'
    			);
			});
		},
		sortConcerts: function () {
			var sorting = $(".sort:checked");

			if (sorting.data('order') == 'DESC') {
				// For descending sort
				this.sortObj(this.currentConcerts, sorting.val(), true);
			}
			else{
				// For ascending sort
				this.sortObj(this.currentConcerts, sorting.val());
			}
		},
		applyFilters: function () {
			// Get the stored data
			this.currentConcerts = this.concerts;

			// SORTING
			this.sortConcerts();

			/* Date filter */
			if ($("#from").val() != "" && $("#to").val() != "") {
				var from = $("#from").datepicker("getDate");
				var to = $("#to").datepicker("getDate");
				this.filterConcertsByDateFromTo(from, to);
			}
			else{
				$("#filter-date").html("Allir");
			}

			/* Search 'filter' */
			if ($("#search").val() != "") {
				console.log("SEARCH");
				var searchQuery = $("#search").val().toLowerCase();
				this.searchConcertsByName(searchQuery);
			}
			this.updateConcertsTable();
		},
		filterConcertsByDateFromTo: function (fromDate, toDate) {
			$("#filter-date").html(moment(fromDate).format('DD-MM-YYYY') + "->" + moment(toDate).format('DD-MM-YYYY'));
			var concerts = [];
			this.currentConcerts.forEach(function(concert) {
				// Make the concert date string as a Date
				var concertDate = new Date(concert["dateOfShow"]).setHours(0,0,0);
				if (fromDate <= concertDate && concertDate <= toDate) {
					concerts.push(concert);
				}
			});
			this.currentConcerts = concerts;
		},
		searchConcertsByName: function (searchQuery) {
			var concerts = [];
			this.currentConcerts.forEach(function(concert) {
				if (concert["eventDateName"].toLowerCase().indexOf(searchQuery) >= 0) {
					concerts.push(concert);
				}
			});
			this.currentConcerts = concerts;
		},
		/* Sort kóði frá STACKOVERFLOW, ég bætti við reversed */
		sortObj: function (list, key, reversed=false) {
		    function compare(a, b) {
		        a = a[key];
		        b = b[key];
		        var type = (typeof(a) === 'string' ||
		                    typeof(b) === 'string') ? 'string' : 'number';
		        var result;
		        if (type === 'string') result = a.localeCompare(b);
		        else result = a - b;
		        return result;
		    }
		    if (reversed) {
		    	return list.sort(compare).reverse();
		    }
		    return list.sort(compare);
		}
	}

	function API() {

	}

	API.prototype = {
		constructor: API,
		getConcerts: function (success) {
			$.get("http://apis.is/concerts", success);
		}
	}

	var app = new App();
})();